#
# iPKG
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library for the demo programs.
BIN_LIB=IPKG

# Minimum OS version 7.3
TGTRLS=*CURRENT

INCDIR=/usr/local/include

VERSION=0.1.0-b6

#
# User-defined part end
#-------------------------------------------------------------------------------


# suffix rules


all: compile bind commands

compile:
	$(MAKE) -C thirdparty/message $*
	$(MAKE) -C thirdparty/llist $*
	$(MAKE) -C src/ $*

clean:
	$(MAKE) -C src/ clean $*
	$(MAKE) -C thirdparty/message/ clean $*
	$(MAKE) -C thirdparty/llist/ clean $*

bind:
	$(MAKE) -C src/ bind $*

commands:
	$(MAKE) -C src/ commands $*

prerelease:
	system "CRTBNDRPG $(BIN_LIB)/IPKGUSRATR SRCSTMF('thirdparty/updusratr/updusratr.rpgle')"
	system "CALL $(BIN_LIB)/IPKGUSRATR PARM('$(BIN_LIB)' 'IPKG' '*PGM' '$(VERSION)')"
	system "DLTPGM $(BIN_LIB)/IPKGUSRATR"

release: prerelease
	-system "DLTOBJ $(BIN_LIB)/IPKGSAVF OBJTYPE(*FILE)"
	system "CRTSAVF $(BIN_LIB)/IPKGSAVF"
	system "CHGOWN OBJ('/QSYS.LIB/$(BIN_LIB).LIB/IPKG.PGM') NEWOWN(QPGMR)"
	system "CHGOWN OBJ('/QSYS.LIB/$(BIN_LIB).LIB/IPKG.CMD') NEWOWN(QPGMR)"
	system "SAVOBJ OBJ(IPKG) LIB($(BIN_LIB)) DEV(*SAVF) OBJTYPE(*PGM *CMD) SAVF($(BIN_LIB)/IPKGSAVF) TGTRLS($(TGTRLS)) DTACPR(*YES)"
	
.PHONY:
