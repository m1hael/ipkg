**FREE

///
// iPKG : TR Requirements Check
// 
// This modules adds support for TR requirements to package requirements.
//
// TR requirements needs to be expressed for every OS version the package is 
// created for. The OS version is appended to TR (uppercase) in braces ().
//
//     TR(7.3) >= 3
//     TR(7.4) >= 0
//
// The requirement is only checked for the installed OS version.
//
// @author Mihael Schmidt
// @date   13.06.2020
///


ctl-opt nomain;

/include 'ipkgreqtr_h.rpgle'
/include 'ipkgutil_h.rpgle'
/include 'message_h.rpgle'


dcl-proc ipkg_req_doesMeetTrRequirement export;
  dcl-pi *n ind;
    required char(20) const;
    minTrLevel char(5) const options(*nopass);
    type int(10) const options(*nopass);
  end-pi;
  
  dcl-s reqOsVersion char(6);
  dcl-s instOsVersion char(6);
  dcl-s instTrLevel int(10);
  dcl-s errorMessage varchar(1000);
  
  reqOsVersion = retrieveOsVersionFromRequirement(required);
  
  exec sql SELECT PTF_GROUP_TARGET_RELEASE AS OS_RELEASE, PTF_GROUP_LEVEL AS TR_LEVEL 
           INTO :instOsVersion, :instTrLevel
           FROM QSYS2.GROUP_PTF_INFO 
           WHERE PTF_GROUP_DESCRIPTION = 'TECHNOLOGY REFRESH' 
             AND PTF_GROUP_STATUS = 'INSTALLED' 
           ORDER BY PTF_GROUP_TARGET_RELEASE DESC 
           FETCH FIRST 1 ROWS ONLY;
  if (sqlcode = 0);
    if (reqOsVersion = instOsVersion);
      return ipkg_util_assertDependency(minTrLevel : %char(instTrLevel) : type);
    else;
      return *on;
    endif;
  else;
    message_info('Could not determine current TR level.');
    exec sql GET DIAGNOSTICS CONDITION 1 :errorMessage = MESSAGE_TEXT;
    message_info('SQL Code: ' + %char(sqlcode) + ' - ' + errorMessage);
    return *off;
  endif;
end-proc;


dcl-proc retrieveOsVersionFromRequirement;
  dcl-pi *n char(6);
    s char(20) const;
  end-pi;

  dcl-ds version likeds(ipkg_util_version_t) inz;
  dcl-s versionString char(6);
  dcl-s x int(10);
  dcl-s x2 int(10);
  
  x = %scan('(' : s);
  x2 = %scan(')' : s);
  if (x = 0 or x2 = 0);
    return *blank;
  endif;
  
  version = ipkg_util_parsePackageVersion(%subst(s : x+1 : x2 - x - 1));
  if (version.major = 0);
    return *blank;
  endif;
  
  versionString = ipkg_util_versionToString(version);
  
  return versionString;
end-proc;
