**FREE

///
// iPKG : Cryptographic Procedures
//
// @author Mihael Schmidt
// @date 25.04.2020
///


ctl-opt nomain;

/if defined(THREAD_SAFE)
ctl-opt thread(*CONCURRENT);
/endif


/include 'ipkgcrypto_h.rpgle'
/include 'ifsio_h.rpgle'
/include 'system_h.rpgle'


dcl-proc ipkg_crypto_md5 export;
  dcl-pi *n char(32);
    input varchar(65535);
    pFinalFlag ind options(*nopass) const;
  end-pi;

  dcl-s hash char(50);
  dcl-ds algd0100 likeds(algd0100_t) inz;
  dcl-ds algd0500 likeds(algd0500_t);
  dcl-s output char(50);
  dcl-s contextToken char(8) static inz;
  dcl-s finalFlag ind inz(*on);
  dcl-ds errorCode likeds(qusec);
  
  clear errorCode;
  
  if (%parms() = 2 and not pFinalFlag);
    finalFlag = *off;
  endif;

  if (contextToken = *blank);
    algd0500.algorithm = ALGORITHM_MD5;
    sys_createAlgorithmContext(algd0500 : 'ALGD0500' : contextToken : errorCode);
  endif;

  algd0100.contextToken = contextToken;
  algd0100.finalFlag = '0';
  if (finalFlag);
    algd0100.finalFlag = '1';
  endif;

  clear errorCode;
  sys_calculateHash(
      %addr(input : *DATA) :
      %len(input) :
      'DATA0100' :
      algd0100 :
      'ALGD0100' :
      '0' :        // crypto auto select device
      *blank :     // crypto device
      hash :
      errorCode);

  if (finalFlag);
    clear errorCode;
    sys_destroyAlgorithmContext(contextToken : errorCode);
    contextToken = *blank;
  endif;

  cvthc(output : hash : %len(output));
  return %subst(output : 1 : 32);
end-proc;


dcl-proc ipkg_crypto_md5file export;
  dcl-pi *n char(32);
    path char(10000) const;
  end-pi;

  dcl-c BLOCK_SIZE 65535;
  dcl-s data varchar(BLOCK_SIZE);
  dcl-s hash char(32);
  dcl-s finalFlag ind inz(*off);
  dcl-s fd int(10);
  dcl-s bytesRead int(10);
  
  fd = open(%trimr(path) : O_RDONLY);
  if (fd < 0);
    return *blank;
  endif;
  
  %len(data) = BLOCK_SIZE;
  bytesRead = read(fd : %addr(data : *DATA) : BLOCK_SIZE);
  dow (bytesRead > 0);
    %len(data) = bytesRead;
    
    if (bytesRead < BLOCK_SIZE);
      finalFlag = *on;
    endif;
    
    hash = ipkg_crypto_md5(data : finalFlag);
    
    %len(data) = BLOCK_SIZE;
    bytesRead = read(fd : %addr(data : *DATA) : BLOCK_SIZE);
  enddo;

  callp close(fd);

  return hash;
end-proc;

