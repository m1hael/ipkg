# iPKG #
iPKG provides a full package management system for the IBM i platform. The main 
program is a native client (written in RPG) which can install one or more packages 
from various sources (local or the internet). 

Wouldn't it be nice to just enter one line on the console to install a program 
with all its dependencies?

The goal of this project is to provide a native client in compiled form to just 
download and run.

## What Software can I install with this client? ##
The idea is to package all open source software available for IBM i platform.

The following projects might be candidates to be packaged:

* JSON service program
* C headers for the standard C functions
* Linked list service program
* DSM (dynamic screen manager)
* HTTP client
* STOMP client
* ...

If your project is not listed here and you would like to get your software packaged 
then just drop me a mail, mihael@rpgnextgen.com .

## Installation ##
Just download the save file from the download section and restore the save file 
in the library you want. Don't forget to check the md5 hash to verify the file 
integrity.

```
CRTSAVF MY_LIB/IPKGSAVF
```


```
wget https://bitbucket.org/m1hael/ipkg/downloads/ipkgclient.savf
wget https://bitbucket.org/m1hael/ipkg/downloads/ipkgclient.md5
md5sum -c ipkgclient.md5
cp ipkgclient.savf /QSYS.LIB/MY_LIB.LIB/IPKGSAVF.FILE
```

```
RSTOBJ OBJ(IPKG) SAVLIB(IPKG) DEV(*SAVF) SAVF(*LIBL/IPKGSAVF) RSTLIB(MY_LIB)
```


## Contribution guidelines ##

You can contribute in any way you like. Just do it! =)

Take a look at the wiki to get into the project.

## Who do I talk to? ##

Mihael Schmidt , mihael@rpgnextgen.com