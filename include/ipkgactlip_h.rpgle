**FREE

/if not defined (IPKGACTLIP)
/define IPKGACTLIP

dcl-pr ipkg_action_listInstalledPackages extproc(*dclcase);
  ipkgLibrary char(10) const;
end-pr;

dcl-pr ipkg_action_listAvailablePackages extproc(*dclcase);
  ipkgLibrary char(10) const;
  repository varchar(100) const options(*nopass);
end-pr;

/endif