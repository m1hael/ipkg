**FREE

/if not defined (IPKGACTINF)
/define IPKGACTINF

/include 'ipkgio_h.rpgle'

dcl-pr ipkg_action_showPackageInfo extproc(*dclcase);
  filePath like(ipkg_path_t) const;
end-pr;

/endif
