**FREE

/if not defined (SYSAPI)
/define SYSAPI

/include 'qusec_h.rpgle'

dcl-ds sys_convert_case_control_block_t qualified template;
  requestType int(10) inz(1);
  ccsid int(10) inz(0);
  convertTo int(10) inz(0);
  reserved char(10) inz(*ALLX'00');
end-ds;

dcl-pr sys_getObjectDescription extpgm('QUSROBJD');
  receiver char(1000);
  length int(10) const;
  format char(10) const;
  qualifiedObjectName char(20) const;
  type char(10) const;
end-pr;

dcl-pr sys_executeCommand extpgm('QCMDEXC');
  command char(1000) const;
  length packed(15 : 5) const;
end-pr;

dcl-pr sys_convertCase extproc('QlgConvertCase');
  controlBlock likeds(sys_convert_case_control_block_t) const;
  inString char(65535) const options(*varsize);
  outString char(65535) options(*varsize);
  length int(10) const;
  qusec likeds(qusec);
end-pr;

dcl-pr sys_checkTargetRelease extpgm('QSZCHKTG');
  targetRelease char(10) const;
  supportedReleases char(10) options(*varsize) const;
  numberSupportedReleases int(10) const;
  validatedTargetRelease char(6);
  supportedRelease char(6);
  errorCode likeds(qusec);
end-pr;

dcl-pr cvthc extproc('cvthc');
  target char(65534) options(*varsize);
  src_bits char(32767) options(*varsize) const;
  tgt_length int(10) value;
end-pr;


dcl-ds algd0100_t qualified template;
  contextToken char(8);
  finalFlag char(1);
end-ds;

dcl-ds algd0500_t qualified template;
  algorithm int(10);
end-ds;

dcl-c ALGORITHM_MD5 1;
dcl-c ALGORITHM_SHA1 2;
dcl-c ALGORITHM_SHA256 3;

dcl-pr sys_createAlgorithmContext extproc('Qc3CreateAlgorithmContext');
  input char(1000) options(*varsize) const;
  format char(8) const;
  contextToken char(8);
  errorCode likeds(qusec);
end-pr;

dcl-pr sys_destroyAlgorithmContext extproc('Qc3DestroyAlgorithmContext');
  contextToken char(8);
  errorCode likeds(qusec);
end-pr;

dcl-pr sys_calculateHash extproc('Qc3CalculateHash');
  input pointer value;
  inputLength int(10) const;
  inputDataFormat char(8) const;
  algorithm char(65535) const options(*varsize);
  algorithmFormat char(8) const;
  cryptoServiceProvier char(1) const;
  cryptoDeviceName char(10) const;
  hash char(65535) options(*varsize);
  errorCode likeds(qusec);
end-pr;

/endif
