**FREE

/if not defined (IPKGCRYPTO)
/define IPKGCRYPTO

dcl-pr ipkg_crypto_md5 char(32) extproc(*dclcase);
  input varchar(65535);
  finalFlag ind options(*nopass) const;
end-pr;

dcl-pr ipkg_crypto_md5file char(32) extproc(*dclcase);
  path char(10000) const;
end-pr;

/endif
