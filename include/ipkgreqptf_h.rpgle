**FREE

/if not defined (IPKGREQPTF)
/define IPKGREQPTF

dcl-pr ipkg_req_doesMeetPtfRequirement ind extproc(*dclcase);
  requiredPtf char(20) const;
  version char(5) const options(*nopass);
  type int(10) const options(*nopass);
end-pr;

/endif
