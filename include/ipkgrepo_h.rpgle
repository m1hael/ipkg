**FREE

/if not defined (IPKGREPO)
/define IPKGREPO

dcl-ds ipkg_repo_t qualified template;
  updateRepository pointer(*proc);
  retrievePackage pointer(*proc);
end-ds;


dcl-pr ipkg_repo_updateRepository extproc(*dclcase);
  ipkgLibrary char(10) const;
  repoId int(10) const;
  repoUrl varchar(1000) const;
  repoImpl pointer const;
end-pr;

dcl-pr ipkg_repo_retrievePackage varchar(1000) extproc(*dclcase);
  ipkgLibrary char(10) const;
  repoUrl varchar(1000) const;
  packageUrl varchar(1000) const;
  repoImpl pointer const;
end-pr;

dcl-pr ipkg_repo_finalize extproc(*dclcase);
  repoImpl pointer;
end-pr;

dcl-pr ipkg_repo_getPrimaryMetaDataFilePath varchar(1000) extproc(*dclcase);
  repoMetaDataPath varchar(1000) const;
end-pr;

dcl-pr ipkg_repo_processPrimaryMetaData extproc(*dclcase);
  ipkgLibrary char(10) const;
  repoId int(10) const;
  filePath varchar(1000) const;
end-pr;

dcl-pr ipkg_repo_getRepositoryProvider pointer extproc(*dclcase);
  url varchar(1000) const;
end-pr;

/endif
