**FREE

ctl-opt dftactgrp(*no) actgrp(*caller) main(main);


dcl-ds version_t qualified template;
  major int(10);
  minor int(10);
  revision int(10);
  build int(10);
end-ds;


/include '../include/libc_h.rpgle'


dcl-proc main;
  
  dcl-ds version likeds(version_t);
  dcl-s s char(20);
  
  dsply %char(comparePackageVersion('2.8.3-1' : '2.11.2-1'));
  
end-proc;


dcl-proc comparePackageVersion export;
  dcl-pi *n int(10);
    version1 char(20) const;
    version2 char(20) const;
  end-pi;

  dcl-ds v1 likeds(version_t);
  dcl-ds v2 likeds(version_t);
  dcl-s rc int(10);
  
  v1 = parsePackageVersion(version1);
  v2 = parsePackageVersion(version2);

  if (v1 = v2);
    return 0;
  endif;
  
  rc = compareInteger(v1.major : v2.major);
  if (rc = -1);
    return -1;
  elseif (rc > 0);
    return 1;
  endif;
  
  rc = compareInteger(v1.minor : v2.minor);
  if (rc = -1);
    return -1;
  elseif (rc > 0);
    return 1;
  endif;
  
  rc = compareInteger(v1.revision : v2.revision);
  if (rc = -1);
    return -1;
  elseif (rc > 0);
    return 1;
  endif;
  
  return compareInteger(v1.build : v2.build);
end-proc;


dcl-proc compareInteger;
  dcl-pi *n int(10);
    i1 int(10) const;
    i2 int(10) const;
  end-pi;

  return i2 - i1;
end-proc;


dcl-proc parsePackageVersion export;
  dcl-pi *n likeds(version_t);
    s char(20) const;
  end-pi;

  dcl-ds version likeds(version_t) inz;
  dcl-s delimiter char(3);
  dcl-s start int(10);
  dcl-s x int(10);
  dcl-s string char(21);
  
  string = s + x'00';
  delimiter = '.-' + x'00';
  
  x = strcspn(%addr(string) : %addr(delimiter));
  if (x = %len(s));
    // not found
    version.major = %int(s);
    return version;
  endif;
  
  version.major = %int(%subst(s : 1 : x));
  
  start += x + 1;
  x = strcspn(%addr(string) + start : %addr(delimiter));
  if (x = %len(s) - start);
    // not found
    version.minor = %int(%subst(s : start+1));
    return version;
  endif;
  
  version.minor = %int(%subst(s : start+1 : x));
  
  start += x + 1;
  x = strcspn(%addr(string) + start : %addr(delimiter));
  if (x = %len(s) - start);
    // not found
    version.revision = %int(%subst(s : start+1));
    return version;
  endif;
  
  version.revision = %int(%subst(s : start+1 : x));
  
  start += x + 1;
  version.build = %int(%subst(s : start+1));
  
  return version;
end-proc;